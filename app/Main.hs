module Main where

import Text.Regex.TDFA

import System.Console.GetOpt
import System.Console.ANSI
import System.Environment
import System.Directory

import Data.Maybe ( fromMaybe )
import Data.List ( sort, intersperse )


progName :: String
progName = "fper"

progVersion :: String
progVersion = "0.1.0"

{-/////////// START argument parser ///////////-}
data Options = Options
  { optVerbose     :: Bool
  , optHelp        :: Bool
  , optShowVersion :: Bool
  , optLocation    :: String
  , optPattern     :: String
  } deriving Show

startOptions :: Options
startOptions = Options
  { optVerbose       = False
  , optHelp          = False
  , optShowVersion   = False
  , optLocation      = "."
  , optPattern       = "."
  }

options :: [OptDescr (Options -> Options)]
options = 
  [ Option ['v'] ["verbose"]
    (NoArg (\opts -> opts { optVerbose = True }))
    "chatty output message"
  , Option ['h'] ["help"]
    (NoArg (\opts -> opts { optHelp = True }))
    "show this message"
  , Option ['V'] ["version"]
    (NoArg (\opts -> opts { optShowVersion = True }))
    "print software version"
  ]

compilerOpts :: [String] -> IO (Options, [String])
compilerOpts argv =
  case getOpt Permute options argv of
    (o, n,[])   -> return (foldl (flip id) startOptions o, n)
    (_,_,errs) -> ioError (userError (concat errs ++ usageInfo header options))
  where header = "Usage: " ++ progName ++ " [OPTION...] PATH PATTERN"

{-/////////// END argument parser ///////////-}

printHelp :: IO ()
printHelp = putStrLn $ usageInfo ("Usage: " ++ progName ++ " [OPTION...] PATH PATTERN") options

printVersion :: IO ()
printVersion = putStrLn progVersion


-- main pure function to search in file
-- TODO: use filter
search :: String -> String -> IO ()
search loc patt = do
    res <- getDirectoryContents loc
    let res' = filter (\i -> i =~ patt :: Bool) res
    disp loc (sort res')

disp :: String -> [String] -> IO ()
disp _ []    = return ()
disp loc [s] = do
    let loc' = if last loc == '/' then loc else loc ++ "/"
    isDir <- doesDirectoryExist (loc' ++ s)
    if isDir then dispDir s else putStrLn s
disp loc (s:ss) = do
    disp loc [s]
    disp loc ss

dispDir :: String -> IO ()
dispDir s = do
    setSGR [SetColor Foreground Vivid Blue]
    putStrLn s
    setSGR [Reset]

detPatt :: [String] -> (String, String)
detPatt [] = (".", ".")
detPatt [x] = (".", x)
detPatt [x,y] = (x, y)
detPatt (x:y:_) = (x, y)

parseOpts :: Options -> String -> String -> IO ()
parseOpts opts loc patt
  | optHelp opts        = printHelp
  | optShowVersion opts = printVersion
  | otherwise           = search loc patt


main :: IO ()
main = do
    argv <- getArgs
    res <- compilerOpts argv
    let (o, s) = res
        (loc,patt) = detPatt s
        opts = startOptions { optLocation = loc, optPattern = patt }
    parseOpts o (optLocation opts) (optPattern opts)
